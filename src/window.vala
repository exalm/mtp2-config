[GtkTemplate (ui = "/org/gnome/gitlab/alicem/MagicTrackpadConfig/window.ui")]
public class MagicTrackpadConfig.Window : Adw.ApplicationWindow {
    private const int APPLE_VENDOR_ID = 0x05ac;
    private const int MAGIC_TRACKPAD_2_ID = 0x0265;

    [GtkChild]
    private unowned Gtk.CheckButton light_radio;
    [GtkChild]
    private unowned Gtk.CheckButton medium_radio;
    [GtkChild]
    private unowned Gtk.CheckButton firm_radio;

    public enum ClickType {
        LIGHT,
        MEDIUM,
        FIRM
    }

    public ClickType click_type { get; set; }
    public bool silent_click { get; set; }

    public Window (Gtk.Application app) {
        Object (application: app);
    }

    private void apply () {
        uint8 iface_number = 2;

        uint8[] press, release;

        switch (click_type) {
            case LIGHT:
                press = { 0x15, 0x04, 0x04 };
                release = { 0x10, 0x00, 0x00 };
                break;
            case MEDIUM:
                press = { 0x17, 0x06, 0x06 };
                release = { 0x14, 0x00, 0x00 };
                break;
            case FIRM:
                press = { 0x1e, 0x08, 0x08 };
                release = { 0x18, 0x02, 0x02 };
                break;
            default:
                assert_not_reached ();
        }

        if (silent_click)
            press[1] = press[2] = release[1] = release[2] = 0;

        uint8[] press_data =   { 0x22, 0x01, press[0],   0x78, 0x02, press[1],   0x24, 0x30, 0x06, 0x01, press[2],   0x18, 0x48, 0x13 };
        uint8[] release_data = { 0x23, 0x01, release[0], 0x78, 0x02, release[1], 0x24, 0x30, 0x06, 0x01, release[2], 0x18, 0x48, 0x13 };

        assert (press_data.length == 14);
        assert (release_data.length == 14);

        LibUSB.Context context;
        LibUSB.Context.init (out context);

        var devices = context.get_device_list ();

        foreach (var device in devices) {
            var desc = LibUSB.DeviceDescriptor (device);

            if (desc.idVendor != APPLE_VENDOR_ID)
                continue;

            if (desc.idProduct != MAGIC_TRACKPAD_2_ID)
                continue;

            // Found a magic trackpad

            LibUSB.ConfigDescriptor config;
            var error = device.get_active_config_descriptor (out config);

            if (error != LibUSB.Error.SUCCESS) {
                critical ("%s %s", error.get_name (), error.get_description ());
                continue;
            } else
                message ("Got config descriptor");

            LibUSB.DeviceHandle handle;
            error = device.open (out handle);
            if (error != LibUSB.Error.SUCCESS) {
                critical ("%s %s", error.get_name (), error.get_description ());
                continue;
            } else
                message ("Opened device");

            error = handle.kernel_driver_active (iface_number);
            bool driver_active = (error == 1);
            if (error != 0 && error != 1) {
                critical ("%s %s", error.get_name (), error.get_description ());
                continue;
            } else
                message ("Checked if kernel driver is active");

            if (driver_active) {
                error = handle.detach_kernel_driver (iface_number);
                if (error != LibUSB.Error.SUCCESS) {
                    critical ("%s %s", error.get_name (), error.get_description ());
                    continue;
                } else
                    message ("Detached kernel driver");
            }

            error = handle.claim_interface (iface_number);
            if (error != LibUSB.Error.SUCCESS) {
                critical ("%s %s", error.get_name (), error.get_description ());
                continue;
            } else
                message ("Claimed interface");

            message ("Trying to set press feedback to 0x%02x, 0x%02x, 0x%02x", press[0], press[1], press[2]);
            int result = handle.control_transfer (
                LibUSB.RequestType.CLASS |
                LibUSB.RequestRecipient.INTERFACE |
                LibUSB.EndpointDirection.OUT,
                0x09,
                0x0322,
                2,
                press_data,
                (uint16) press_data.length,
                5000
            );
            if (result != press_data.length)
                critical ("Couldn't set press feedback");

            message ("Trying to set release feedback to 0x%02x, 0x%02x, 0x%02x", release[0], release[1], release[2]);
            result = handle.control_transfer (
                LibUSB.RequestType.CLASS |
                LibUSB.RequestRecipient.INTERFACE |
                LibUSB.EndpointDirection.OUT,
                0x09,
                0x0323,
                2,
                release_data,
                (uint16) release_data.length,
                5000
            );
            if (result != press_data.length)
                critical ("Couldn't set release feedback");

            error = handle.release_interface (iface_number);
            if (error != LibUSB.Error.SUCCESS) {
                critical ("%s %s", error.get_name (), error.get_description ());
                continue;
            } else
                message ("Released interface");

            if (driver_active) {
                error = handle.attach_kernel_driver (iface_number);
                if (error != LibUSB.Error.SUCCESS) {
                    critical ("%s %s", error.get_name (), error.get_description ());
                    continue;
                } else
                    message ("Attached kernel driver");
            }

            message ("Closing device");
        }
    }

    construct {
        apply ();

        notify["click-type"].connect (apply);
        notify["silent-click"].connect (apply);

        light_radio.notify["active"].connect (check_click_type);
        medium_radio.notify["active"].connect (check_click_type);
        firm_radio.notify["active"].connect (check_click_type);

        medium_radio.active = true;
    }

    static construct {
        install_property_action ("config.silent-click", "silent-click");
    }

    private void check_click_type () {
        if (light_radio.active)
            click_type = LIGHT;
        else if (medium_radio.active)
            click_type = MEDIUM;
        else
            click_type = FIRM;
    }
}
